Lexer generator.

A simple tool to transform regex expressions into C FSM.

@ Here we will hopefully implement a Lexer. We will do so based on Regular
expression theory. In the end, this code will output an FSM (Finite State
Machine) capable of anotating and tokenizing the language.

@ First, we will transform our regex expression into a NFA (Non-deterministic
Finite Automaton). For this task we will employ the Thompson algorithm or
Thompson's construction https://en.wikipedia.org/wiki/Thompson%27s_construction.

@heading Thompson algorithm.

The algorithmm is described as follows
(*) The NFA of the empty expression $\varepsilon$ is converted to
=(figure assets/NFA/empty.svg)
(*) The NFA of a single symbol $a$ is given by
=(figure assets/NFA/symbol.svg)
(*) The NFA for a concatenation of expressions $a$ and $b$ ($a \vert b$) is
given by
=(figure concat.svg)
(*) The NFA for a Kleene star is given by
=(figure star.svg)

Throughout this section, we will discuss an actual implementation of this
high level algorithm.

@h Data structures.

As any good algorithm (or implementation of one), we must start with the data
structures involved in them. In the case of NFAs and DFAs, we will need
directed graphs.

@ A transition will be represented by the possible letters of that
transition and a pointer to the destination state
=
typedef struct Transition {
	char *transitions;
	int size;
	struct FA_Node *next;
} Transition;

@ Since NFA and DFA only differ in what transitions are valid (DFA are more
restrictive), the underlying data structure we will use for them wil be the
same. Moreover, the Thompson algorithm guarantees that all of the produced
NFAs and DFAs will only have one start state and one end state.
=
typedef struct FA_Node {
	int n_transitions;
	int anotation;
	Transition *transitions;
} FA_Node;

typedef struct FA {
	int n_states;
	FA_Node *states;
	FA_Node *start;
	FA_Node *end;
} FA;

@ Now that our data structures are clear we can start discussing the algorithm
per se. We are going to use a modification of the
//Shunting-yard algorithm -> https://en.wikipedia.org/wiki/Shunting_yard_algorithm//.
The algorithm needs to be modified because of the implicit concat operand
present in all of the regular expressions.

A simpler implementation (my previous version of the algorithm) first expanded
the implit concat operations and then applyied (almost) standard Shunting-yard.
However it wasted a lot of memory while not being much simpler to comprehend.

=(text to TODOS)
	TODO: handle utf8 characters!
=

The variable |last_is_nfa| will be set to true if the last value used was a nfa.
Therefore there will be an implicit concat whenever the last value used was
a NFA and the current value also opens an NFA (either a letter, |[| or |(|).

@=
FA nfa_from_regex(SV regex) {
	String_Builder operator_stack = {0};
	FA_da nfa_stack = {0};
	int last_is_nfa = 0;

	for (size_t i = 0; i < regex.len; i++) {
		char c = regex.data[i];
		if (c == '*' | c == '|') {
			while (operator_stack.len >= 0 && operator_stack.data[0] != '(' &&
			       precedence(operator_stack.data[0]) >= precedence(c)) {
				char operator = da_pop(operator_stack);
				@<Apply operator@>;
			}
			da_push(operator_stack, c);
		} else if (c == '(') {
			da_push(operator_stack, c);
		} else if (c == ')') {
			/* Assert operator_stack.len > 0. Otherwise there is a syntax error in the regex */
			while (operator_stack.data[0] != '(') {
				char operator = da_pop(operator_stack);
				@<Apply operator@>;
			}
			assert(operator_stack.data[0] == '(');
			da_pop(operator_stack);
		}
	}
}

@=
#include <stdio.h>
int main() {
	printf("Hello world!\n");
	return 0;
}
