.PHONY: all web source inweb

all: inweb/Tangled/inweb web source

web:
	mkdir -p public
	cp -r assets public
	inweb/Tangled/inweb -colony colony.txt -member selang -weave

source:
	inweb/Tangled/inweb selang -tangle 1/lg
	clang -std=c2x -Wall -Wextra -pedantic -x c -o lexer_generator "selang/Tangled/Lexer generator.w"

inweb: inweb/Tangled/inweb

inweb/Tangled/inweb:
	bash inweb/scripts/first.sh linux
