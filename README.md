# SELang
The is the programming language that I created because I felt like.

Some ideas are:
* Learn Z3 (or some other SMT theorem prover) for proving memory access
* Better macro support.
* Same compile/run code => Integrated build process.
* Heavy pattern match capabilities.
* Don't use types but traits.
* Polymorphic procedures.
* Memory management with ownership and passed allocators.
* Fine inline control.
* Defer statement.
* Optional types.

We will see how it goes.
Also the idea is for this language to be inherently literate (see
Literate Programming) and we will have to develop a debugger and an IDE?
for it. That for sure has to be fun.
